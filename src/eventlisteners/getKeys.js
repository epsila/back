import { decode } from "notepack.io";
import { commandOptions } from "redis";

import { devices, pseudos } from "../service/redis.js";
import lifeCircleSocket from "../service/lifeCircleSocket.js";

lifeCircleSocket.on("auth", (socket) => {
  socket.on("getKeys", async (pseudo, callback) => {
    console.log(pseudo);
    if (!pseudo) {
      callback(400);
    }
    const ids = await pseudos.sMembers(
      commandOptions({ returnBuffers: true }),
      pseudo.toLowerCase(),
    );
    console.log(ids);
    let keys = await ids
      .reduce(
        (rec, id) => rec.get(commandOptions({ returnBuffers: true }), id),
        devices.multi(),
      )
      .exec();

    if (!keys) {
      callback(404);
      return;
    }
    keys = keys.map((data) => decode(data).key);
    // console.log(datas)
    callback(200, { keys, ids });
  });
});

lifeCircleSocket.on("unauth", (socket) => {
  socket.removeAllListeners("getKeys");
});
