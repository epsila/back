import { decode } from "notepack.io";
import { commandOptions } from "redis";

import { devices } from "../service/redis.js";
import lifeCircleSocket from "../service/lifeCircleSocket.js";

lifeCircleSocket.on("connection", (socket) => {
  socket.on("getKey", async (id, callback) => {
    if (!id) {
      callback(400);
    }
    if (!Buffer.isBuffer(id)) {
      // eslint-disable-next-line no-param-reassign
      id = Buffer.from(id, "base64url");
    }
    let data = await devices.get(commandOptions({ returnBuffers: true }), id);
    if (!data) {
      callback(404);
      return;
    }
    data = decode(data);
    callback(200, data.key);
  });
});
