import { encode } from "notepack.io";
import { commandOptions } from "redis";

import { devices, pseudos, accounts } from "../service/redis.js";
import lifeCircleSocket from "../service/lifeCircleSocket.js";
import { connecters } from "../service/store.js";

lifeCircleSocket.on("connection", (socket) => {
  socket.on("newUser", async (data, callback) => {
    const result = await pseudos.sMembers(
      commandOptions({ returnBuffers: true }),
      data.pseudo.toLowerCase(),
    );
    if (result.length) {
      const idDistant = result.find(
        (key) => connecters[key.toString("base64url")],
      );
      callback(409, idDistant);
      console.info(
        `${data.pseudo}(${data.id.toString("base64url")}) newUser 409`,
      );
      return;
    }
    await Promise.all([
      pseudos.sAdd(data.pseudo.toLowerCase(), data.id),
      accounts.sAdd(data.idAccount ?? data.id, data.id),
      devices.set(
        data.id,
        encode({ ...data, idAccount: data.idAccount ?? data.id }),
        "NX",
      ),
    ]);
    callback(200);
    console.info(
      `${data.pseudo}(${data.id.toString("base64url")}) newUser 200`,
    );
  });
});
