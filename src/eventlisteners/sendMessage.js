import { commandOptions } from "redis";

import lifeCircleSocket from "../service/lifeCircleSocket.js";
import { socketId } from "../service/store.js";
import { set } from "../service/saveMessage.js";
import { accounts } from "../service/redis.js";

lifeCircleSocket.on("auth", (socket) => {
  socket.on("sendMessage", async (data) => {
    if (data.destinationType === "general") {
      socket.to("/auth").emit("message", {
        data: data.data,
        source: {
          id: socket.user.id,
          pseudo: socket.user.pseudo,
          idAccount: socket.user.idAccount,
        },
      });
      console.info(
        `"${data.message}" de ${socket.user.pseudo}(${socket.user.id}) vers general`,
      );
      return;
    }
    let destinations = data.destination;
    if (data.destinationType === "account") {
      destinations = await accounts.sMembers(
        commandOptions({ returnBuffers: true }),
        Buffer.from(data.destination, "base64url"),
      );
    }
    console.log(
      "destinations",
      data.destination,
      data.destinationType,
      destinations,
    );
    const { sendToSocketId, sendToId } = destinations
      .filter((id) => id)
      .reduce(
        (accu, id) => {
          const idString = id.toString("base64url");
          console.log("idString", idString);
          const aSocketId = socketId[idString];
          if (aSocketId) {
            if (socket.id !== aSocketId) {
              accu.sendToSocketId.push(aSocketId);
            }
          } else {
            accu.sendToId.push(idString);
          }
          return accu;
        },
        { sendToSocketId: [], sendToId: [] },
      );

    if (data.persist) {
      await Promise.all(
        sendToId.map((id) =>
          set(id, {
            data: data.data,
            source: {
              id: socket.user.idBin,
              idAccount: socket.user.idAccountBin,
              pseudo: socket.user.pseudo,
            },
          }),
        ),
      );
    }
    if (sendToSocketId.length) {
      socket.to(sendToSocketId).emit("message", {
        data: data.data,
        source: {
          id: socket.user.idBin,
          idAccount: socket.user.idAccountBin,
          pseudo: socket.user.pseudo,
        },
      });
      console.info(
        `${socket.user.pseudo} envoi ${JSON.stringify(
          data,
        )} à ${sendToSocketId}`,
      );
    }
  });
});

lifeCircleSocket.on("unauth", (socket) => {
  socket.removeAllListeners("sendMessage");
});
