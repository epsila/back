import { encode } from "notepack.io";

import { devices, pseudos, accounts } from "../service/redis.js";

import lifeCircleSocket from "../service/lifeCircleSocket.js";
import { socketId } from "../service/store.js";
import io from "../service/socket.io.js";

lifeCircleSocket.on("connection", (socket) => {
  socket.on("requestCreateDevice", async (data, callback) => {
    const idSocket = socketId[data.idDistant.toString("base64url")];

    if (idSocket) {
      const dataResponce = await io.sockets.sockets
        .get(idSocket)
        .emitP("requestCreateDevice", {
          ...data.data,
          id: data.id,
          key: data.key,
        });
      if (dataResponce.success) {
        await Promise.all([
          pseudos.sAdd(data.pseudo.toLowerCase(), data.id),
          accounts.sAdd(dataResponce.idAccount, data.id),
          devices.set(
            data.id,
            encode({
              pseudo: data.pseudo,
              key: data.key,
              idAccount: dataResponce.idAccount,
            }),
            "NX",
          ),
        ]);
        callback(200, dataResponce);
        console.info(
          `${data.pseudo}(${data.id.toString(
            "base64url",
          )}) requestCreateDevice 200`,
        );
      } else {
        callback(400, dataResponce);
        console.info(
          `${data.pseudo}(${data.id.toString(
            "base64url",
          )}) requestCreateDevice 400`,
        );
      }
      return;
    }

    // await Promise.all([
    //   pseudos.sAdd(data.pseudo, id),
    //   users.set(
    //     id,
    //     JSON.stringify({
    //       pseudo: data.pseudo,
    //       key: data.key.toString("base64"),
    //     }),
    //     "NX",
    //   ),
    // ]);
    callback(400);
    console.info(
      `${data.pseudo}(${data.id.toString(
        "base64url",
      )}) requestCreateDevice 400`,
    );
  });
});
