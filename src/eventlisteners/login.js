import crypto from "node:crypto";
import { decode } from "notepack.io";
import { commandOptions } from "redis";

import { devices } from "../service/redis.js";
import lifeCircleSocket from "../service/lifeCircleSocket.js";

lifeCircleSocket.on("connection", (socket) => {
  socket.on("login", async (idBin, callback) => {
    if (!idBin) {
      console.info(`?(?) login 400`);
      callback(400);
      return;
    }
    const id = idBin.toString("base64url");
    let data = await devices.get(
      commandOptions({ returnBuffers: true }),
      idBin,
    );
    if (!data) {
      console.info(`?(${id}) login 404`);
      callback(404);
      return;
    }
    data = decode(data);
    socket.user = {
      ...data,
      id,
      idBin,
      idAccount: data.idAccount.toString("base64url"),
      idAccountBin: data.idAccount,
      authToken: crypto.randomBytes(129),
    };
    const encryptToken = crypto.publicEncrypt(
      {
        key: `-----BEGIN PUBLIC KEY-----
${data.key.toString("base64")}
-----END PUBLIC KEY-----`,
        oaepHash: "SHA512",
      },
      socket.user.authToken,
    );

    callback(200, encryptToken);
    console.info(`${socket.user.pseudo}(${id}) login 200`);
  });
});
