import { createWriteStream, createReadStream } from "node:fs";
import { stat, truncate, rm } from "node:fs/promises";
import { encode, decode } from "notepack.io";

const filesW = {};

export const get = async (id) => {
  const path = `data/${id}`;
  try {
    await stat(path);
  } catch (e) {
    return [];
  }
  const dataReturn = [];
  let afterData;
  const readable = createReadStream(path);
  let size;
  readable.on("data", (data) => {
    let offset = 0;
    if (afterData === undefined) {
      size = data.readUInt32BE();
      offset += 4;
    } else if (size === 0) {
      const bufferConcat = Buffer.concat([afterData, data], 4);
      size = bufferConcat.readUInt32BE(offset);
      offset += 4 - afterData.length;
    } else {
      if (afterData.length + data.length < size) {
        afterData = Buffer.concat([afterData, data]);
        return;
      }
      const bufferConcat = Buffer.concat([afterData, data], size);
      dataReturn.push(decode(bufferConcat));
      offset += size - afterData.length;
      if (offset + 4 > data.length) {
        size = 0;
        afterData = data.subarray(offset);
        return;
      }
      size = data.readUInt32BE(offset);
      offset += 4;
    }
    while (size + offset <= data.length) {
      dataReturn.push(decode(data.subarray(offset, offset + size)));
      offset += size;
      if (offset + 4 > data.length) {
        size = 0;
        break;
      }
      size = data.readUInt32BE(offset);
      offset += 4;
    }
    afterData = data.subarray(offset);
  });
  return new Promise((resolve, reject) => {
    readable.on("error", reject);
    readable.on("end", () => {
      truncate(path).then(() => {
        resolve(dataReturn);
      });
    });
  });
};
export const set = async (id, data) => {
  if (!filesW[id]) {
    filesW[id] = createWriteStream(`data/${id}`, { flags: "a" });
  }
  const dataBinary = encode(data);
  const length = Buffer.allocUnsafe(4);
  length.writeUInt32BE(dataBinary.length);
  return new Promise((resolve, reject) => {
    filesW[id].write(Buffer.concat([length, dataBinary]), (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
};
export const del = async (id) => {
  if (filesW[id]) {
    await new Promise((resolve, reject) => {
      filesW[id].close((error) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(rm(`data/${id}`, { force: true }));
      });
    });
  }
  await rm(`data/${id}`, { force: true });
};

/*
const { randomBytes } = await import("node:crypto");

(async () => {
  for (let y = 0; y < 1000; y++) {
    console.log("y", y);
    for (let i = 0; i < 10000; i++) {
      await set("test", {
        data: randomBytes(Math.random() * 512 + 256),
        i,
      });
    }
    console.log("y", y);
    const data = await get("test");
    for (let i = 0; i < data.length; i++) {
      if (data[i].i !== i) {
        console.log("nook!!", data[i].i, i);
      }
    }
  }
})();
*/
/*
(async () => {
  const data = await get("test");
  for (let i = 0; i < data.length; i++) {
    if (data[i].i !== i) {
      console.log("nook!!", data[i].i, i);
    }
  }
})();
*/
