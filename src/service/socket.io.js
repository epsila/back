import { Server } from "socket.io";
import parser from "socket.io-msgpack-parser";

const option = {};
if (process.env.NODE_ENV === "production") {
  option.parser = parser;
} else {
  option.cors = {
    origin: ["http://localhost:8000", "http://localhost:8001"],
  };
}

export default new Server(3000, option);

console.info("listen port 3000");
