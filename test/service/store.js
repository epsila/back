import { describe, it } from 'node:test';
import assert from 'node:assert/strict';

import { connecters, socketId } from "../../src/service/store.js";

const { randomBytes } = await import("node:crypto");



describe('service/store', () => {
  it('test connecters', ()=>{
    const key = randomBytes(33).toString("base64url")
    const valueKey = randomBytes(33).toString("base64url")
    const valueTest = randomBytes(33).toString("base64url")

    connecters[key]=valueKey;
    assert.deepStrictEqual(valueKey,connecters[key])
    connecters.test=valueTest;
    assert.deepStrictEqual(valueTest,connecters.test);
    delete connecters[key];
    delete connecters.test;
    assert.deepStrictEqual(undefined,connecters[key]);
    assert.deepStrictEqual(undefined,connecters.test);

  });

  it('test socketId', ()=>{
    const key = randomBytes(33).toString("base64url")
    const valueKey = randomBytes(33).toString("base64url")
    const valueTest = randomBytes(33).toString("base64url")

    socketId[key]=valueKey;
    assert.deepStrictEqual(valueKey,socketId[key])
    socketId.test=valueTest;
    assert.deepStrictEqual(valueTest,socketId.test);
    delete socketId[key];
    delete socketId.test;
    assert.deepStrictEqual(undefined,socketId[key]);
    assert.deepStrictEqual(undefined,socketId.test);

  });
});

